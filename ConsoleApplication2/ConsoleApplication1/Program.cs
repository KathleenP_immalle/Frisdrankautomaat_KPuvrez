﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int amountGiven = 100;
            int itemCost = 45;
            int wisselGeld = amountGiven - itemCost;

            string EenEuro = "";
            string VijftigCent = "";
            string TwintigCent = "";
            string TienCent = "";
            string VijfCent = "";
            string TweeCent = "";
            string EenCent = "";

            EenEuro += wisselGeld / 100;
            wisselgeld %= 100;
            Console.WriteLine("Number of 1 euro coins returned is " + EenEuro);
            VijftigCent += wisselGeld / 50;
            wisselGeld %= 50;
            Console.WriteLine("Number of 50 cent coins returned is " + VijftigCent);
            TwintigCent += wisselGeld / 20;
            wisselGeld %= 20;
            Console.WriteLine("Number of 20 cent coins returned is " + TwintigCent);
            TienCent += wisselGeld / 10;
            wisselGeld %= 10;
            Console.WriteLine("Number of 10 cent coins returned is " + TienCent);
            VijfCent += wisselGeld / 5;
            wisselGeld %= 5;
            Console.WriteLine("Number of 5 cent coins returned is " + VijfCent);
            TweeCent += wisselGeld / 2;
            wisselGeld %= 2;
            Console.WriteLine("Number of 2 cent coins returned is " + TweeCent);
            EenCent += wisselGeld / 1;
            wisselGeld %= 1;
            Console.WriteLine("Number of 1 cent coins returned is " + EenCent);
        }
    }
}
