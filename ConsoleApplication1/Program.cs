﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int inworp = 200;
            int itemCost = 110;
            int wisselGeld = inworp - itemCost;
            string code = "";
            code += wisselGeld / 100;
            wisselGeld %= 100;
            code += wisselGeld / 50;
            wisselGeld %= 50;
            code += wisselGeld / 20;
            wisselGeld %= 20;
            code += wisselGeld / 10;
            wisselGeld %= 10;
            code += wisselGeld / 5;
            wisselGeld %= 5;
            code += wisselGeld / 2;
            wisselGeld %= 2;
            code += wisselGeld / 1;
            wisselGeld %= 1;

            var x = code.ToCharArray();            

            Console.WriteLine("Number of 1 euro coins is " + x[0]);
            Console.WriteLine("Number of 50 cent coins is " + x[1]);
            Console.WriteLine("Number of 20 cent coins is " + x[2]);
            Console.WriteLine("Number of 10 cent coins is " + x[3]);
            Console.WriteLine("Number of 5 cent coins is " + x[4]);
            Console.WriteLine("Number of 2 cent coins is " + x[5]);
            Console.WriteLine("Number of 1 cent coins is " + x[6]);
        }
    }
}
